﻿using System.Collections.ObjectModel;
namespace HexChat3.ViewModels;
/// <summary>
/// Main View Model
/// </summary>
public class MainViewModel : BaseViewModel {
    /// <summary>
    /// Tabs
    /// </summary>
    public ObservableCollection<TabItemViewModel> Tabs { get; } = new ObservableCollection<TabItemViewModel>();
    /// <summary>
    /// Selected Tab
    /// </summary>
    private TabItemViewModel selectedTab;
    /// <summary>
    /// Selected Tab
    /// </summary>
    public TabItemViewModel SelectedTab {
        get => selectedTab;
        set => SetProperty(ref selectedTab, value);
    }
}