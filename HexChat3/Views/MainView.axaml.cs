using Avalonia.Controls;
namespace HexChat3.Views;
/// <summary>
/// Main View
/// </summary>
public partial class MainView : UserControl {
    /// <summary>
    /// Constructor
    /// </summary>
    public MainView() {
        InitializeComponent();
    }
}