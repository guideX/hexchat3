﻿using Avalonia.Controls;
namespace HexChat3.Views;
/// <summary>
/// Main Window
/// </summary>
public partial class MainWindow : Window {
    /// <summary>
    /// Constructor
    /// </summary>
    public MainWindow() {
        InitializeComponent();
    }
}