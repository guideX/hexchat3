﻿using System;
using Avalonia;
using Avalonia.ReactiveUI;
namespace HexChat3.Desktop;
class Program {
    /// <summary>
    /// Main
    /// </summary>
    /// <param name="args"></param>
    [STAThread]
    public static void Main(string[] args) => BuildAvaloniaApp().StartWithClassicDesktopLifetime(args);
    /// <summary>
    /// Build Avalonia App
    /// </summary>
    /// <returns></returns>
    public static AppBuilder BuildAvaloniaApp()
        => AppBuilder.Configure<App>()
            .UsePlatformDetect()
            .WithInterFont()
            .LogToTrace()
            .UseReactiveUI();
}